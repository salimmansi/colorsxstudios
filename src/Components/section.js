import React from "react";
import ReactAudioPlayer from "react-audio-player";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

import {
  WaveformVisualizer,
  WaveformVisualizerTheme,
} from "react-audio-visualizers";
const Section = ({
  id,
  backgroundcolor,
  artist_name,
  img,
  descreption,
  link,
}) => {
  return (
    <div
      className="grid grid-rows-4  grid-flow-col gap-4 p-4 h-screen text-gray-600 body-font "
      style={{ backgroundColor: backgroundcolor }}
    >
      <div className="content   row-span-3 col-span-7">
        <div className=" grid grid-cols-3 gap-3 h-full content-center">
          <div
            data-aos="fade-up"
            data-aos-delay="50"
            className=" lg:max-w-lg lg:w-full  md:w-1/2 w-5/6 mb-10 md:mb-0 "
          >
            <img
              className=" w-full lg:h-auto rounded-lg object-cover object-center "
              alt={artist_name}
              src={img}
            />
          </div>
          <div className=" md:items-centre md:text-left my-16 content-left p-5 text-center">
            <h1
              data-aos="fade-down"
              data-aos-offset="300"
              className=" font-Roboto  sm:text-5xl text-2xl mb-4 font-medium text-zinc-800  tracking-wide"
            >
              {id === 0 ? "COLORSXSTUDIOS" : artist_name}
            </h1>
            <p
              data-aos="fade-down"
              data-aos-delay="50"
              className="mb-8  text-base  text-gray-200 leading-relaxed"
            >
              {descreption}
            </p>
            <div data-aos="fade-down" data-aos-delay="100" className="">
              <WaveformVisualizer
                audio={link}
                theme={WaveformVisualizerTheme.squaredBars}
                showLoaderIcon
                showMainActionIcon={true}
                iconsColor="#ffff"
              />
            </div>
          </div>
          <div data-aos="fade-up" data-aos-delay="200">
            <Card className="mx-3 my-4" sx={{ maxWidth: 345 }}>
              <CardMedia
                component="img"
                alt="green iguana"
                height="80"
                image={img}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  New York : 05/22/2022
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  The wonderful Jorja Smith takes us back to where it all began
                </Typography>
              </CardContent>
              <CardActions>
                <Typography size="large">Price</Typography>
                <Typography size="large">Available for purshace</Typography>
              </CardActions>
            </Card>
          </div>
        </div>
      </div>

      <div
        data-aos="fade-down"
        data-aos-delay="250"
        className="row-span-1 col-span-7"
      >
        <div className="container py-12 px-3 mx-auto">
          <div className="flex flex-wrap -m-4 text-center">
            <div className="p-4 sm:w-1/4 w-1/2">
              <h2 className="title-font font-medium sm:text-4xl text-3xl text-gray-900">
                2.7K
              </h2>
              <p className="leading-relaxed">Users</p>
            </div>
            <div className="p-4 sm:w-1/4 w-1/2">
              <h2 className="title-font font-medium sm:text-4xl text-3xl text-gray-900">
                1.8K
              </h2>
              <p className="leading-relaxed">Subscribes</p>
            </div>
            <div className="p-4 sm:w-1/4 w-1/2">
              <h2 className="title-font font-medium sm:text-4xl text-3xl text-gray-900">
                35
              </h2>
              <p className="leading-relaxed">Downloads</p>
            </div>
            <div className="p-4 sm:w-1/4 w-1/2">
              <h2 className="title-font font-medium sm:text-4xl text-3xl text-gray-900">
                4
              </h2>
              <p className="leading-relaxed">Products</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Section;
