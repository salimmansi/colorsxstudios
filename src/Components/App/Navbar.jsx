import React from 'react'

const Navbar = () => {
  return (
    <header className="text-gray-900 body-font">
    <div data-aos="fade-right"
     data-aos-offset="300"
     data-aos-easing="ease-in-sine"
     data-aos-duration="1000" className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
      <a className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
        
        <span className="ml-3 text-xl">ColorsXStudios</span>
      </a>
      <nav className="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
        <a className="mr-5 hover:text-gray-900 tracking-wide ">Home</a>
        <a className="mr-5 hover:text-gray-900 tracking-wide ">News</a>
        <a className="mr-5 hover:text-gray-900 tracking-wide">Artists</a>
        <a className="mr-5 hover:text-gray-900 tracking-wide">Profile</a>
      </nav>
 
    </div>
  </header>

  )
}

export default Navbar