import React, { useRef, useEffect, useState } from "react";
import "./App.css";
import Navbar from "./Navbar";
import "../../Assets/Css/Navbar.css";
import { ReactComponent as Herosvg } from "../../Assets/img/home_img2.svg";
//import { ReactComponent as Billie } from '../../Assets/img/billie.svg';
import bl from "../../Assets/img/billie.svg";
import "aos/dist/aos.css";
import { data } from "../../Assets/js/data";

import Fullpage, {
  FullpageSection,
  FullPageSections,
  FullpageNavigation,
} from "@ap.cx/react-fullpage";
import Section from "../section";

const App = () => {
  const containerRef = useRef(null);
  const [isVisible, setIsVisible] = useState(false);

  const callbackFunction = (entries) => {
    const [entry] = entries;
    setIsVisible(entry.isIntersecting);
  };
  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 1.0,
  };

  useEffect(() => {
    const observer = new IntersectionObserver(callbackFunction, options);
    if (containerRef.current) observer.observe(containerRef.current);

    return () => {
      if (containerRef.current) observer.unobserve(containerRef.current);
    };
  }, [containerRef, options]);

  return (
    <>
      <Fullpage>
        <FullpageNavigation />
        <FullPageSections>
          {data.map((e, index) => {
            return (
              <FullpageSection key={e.artist_name}>
                <Section
                  id={index}
                  backgroundcolor={e.backgroundcolor}
                  artist_name={e.artist_name}
                  descreption={e.descreption}
                  img={e.img}
                  link={e.link}
                  key={index}
                />
              </FullpageSection>
            );
          })}
        </FullPageSections>
      </Fullpage>
    </>
  );
};

export default App;
