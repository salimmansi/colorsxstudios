export const data = [
  {
    backgroundcolor: "rgb(141,116,174)",
    artist_name: "COLORSXSTUDIOD",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/1.png?alt=media&token=9fdf4d86-5c7b-40a6-a317-129cccbba9f5",
    descreption:
      "All COLORS, no genres. COLORS is a unique aesthetic music platform with diverse and exceptional artists from all around the globe.",
    link: "../../sounds/dean.mp3",
  },
  {
    backgroundcolor: "#93B9B1",
    artist_name: "Billie Ellish",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/bilie.PNG?alt=media&token=5eefc554-40fa-4dd7-8f3f-3af2aca981de",
    descreption:
      "Los Angeles-based pop starlet Billie Eilish is back with a stunning and deeply introspective rendition of idontwannabeyouanymore, lifted from her debut EP Don't Smile at Me.",
    link: "../../sounds/billie.mp3",
  },
  {
    backgroundcolor: "#183C56",
    artist_name: "Jorja Smith",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/3.png?alt=media&token=bc27358d-4e97-4f46-8add-5edec7ab64c2",
    descreption:
      "The wonderful Jorja Smith takes us back to where it all began, with a stirring performance of her now-viral hit, Blue Lights.",
    link: "../../sounds/jorja.mp3",
  },
  {
    backgroundcolor: "#4984AE",
    artist_name: "Mahalia",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/4.png?alt=media&token=1bf9f51a-4b62-4013-b5b0-12eac5d6fbbe",
    descreption:
      "Rising British singer Mahalia impresses with her sultry vocals on her sensationally smooth throwback R&B gem and debut single Sober.",
    link: "../../sounds/mahalia.mp3",
  },
  {
    backgroundcolor: "#FBA1EB",
    artist_name: "Doja Cat",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/5.png?alt=media&token=88b9a442-99c1-409b-bf6a-59fc4ddcc72e",
    descreption:
      "L.A. based artist Doja Cat serves up an electrifying performance of ‘Juicy’ (produced by Yeti Beats & Tyson Trax) which is taken from her debut album ‘Amala’",
    link: "../../sounds/doja.mp3",
  },
  {
    backgroundcolor: "#ED7933",
    artist_name: "Siboy",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/6.png?alt=media&token=af74da26-0ad6-4ac8-ba49-8a1c823deb44",
    descreption:
      "Carrying an air of mystery around him, Siboy dons his signature mask to perform an absolutely ruthless and powerful performance of “Au Revoir Merci”, the closing track from his debut album Special.",
    link: "public/sounds/siboy.mp3",
  },
  {
    backgroundcolor: "#C4A19D",
    artist_name: "Angèle",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/7.png?alt=media&token=6bf6848a-3e37-4b88-9165-d4bbcdd70a69",
    descreption:
      "Hypnotic and utterly captivating, Belgian singer/songwriter Angèle graces the COLORS stage with a spellbinding performance of “Ta Reine” (co-produced with Tristan Salvati), taken from her delightful debut album Brol",
    link: "../../sounds/angele.mp3",
  },
  {
    backgroundcolor: "#DB7523",
    artist_name: "Tierra Whack",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/8.png?alt=media&token=ab4af3d9-95ea-4bc7-bd26-5b09fa1030c6",
    descreption:
      "Released as part of her 'WhackHistoryMonth' series, Philly based rapper Tierra Whack serves up a spectacular performance of ‘Unemployed’, which is produced by Kenete Simms",
    link: "../../sounds/tierra.mp3",
  },
  {
    backgroundcolor: "#D1870F",
    artist_name: "Bu Kolthoum",
    img: "https://firebasestorage.googleapis.com/v0/b/colors-19dae.appspot.com/o/9.png?alt=media&token=c5d9b7c7-a5e6-4dc0-a86c-46b05a86a887",
    descreption:
      "Netherlands-based, Syrian rapper, producer & singer Bu Kolthoum embodies the power of ‘Shiva’ on his A COLORS SHOW.",
    link: "../../sounds/bu.mp3",
  },
];
